public abstract class Person {

	/**
	 * @param args
	 */
	String firstName;
	String lastName;
	String gender;
	
	public Person ()
	{}
	
	public Person(String fName, String lName, String gend)//Constructor
	{
		firstName=fName;
		lastName=lName;
		gender=gend;
	}
	
	String getFullName()
	{
		String full;
		full = firstName + " " + lastName;
		return full;
	}

}
abstract class Aircrew extends Person
{
	///Person class
	double salary;
	
	double getSalary()
	{
		return salary;
	}
	
}
class Pilot extends Aircrew
{
	//Pilot Class
	int pilotID;
	
	public Pilot()
	{}
	
	public Pilot(int ID, int sal,String fName, String lName, String gend)
	{
		ID=pilotID;
		salary=sal;
		firstName=fName;
		lastName=lName;
		gender=gend;
	}
	
	int getPilotID()
	{
		return pilotID;
	}
}
class Hostess extends Aircrew
{
	// Hostess Class
	int hostessID;
	
	public Hostess()
	{}
	
	public Hostess( int ID,int sal,String fName, String lName, String gend)
	{
		hostessID=ID;
		salary=sal;
		firstName=fName;
		lastName=lName;
		gender=gend;
	}
	
	void flightAttendatAnnouncemet()
	{
		System.out.println("Fasten Your Seat Belts!");
	}
	
}
class Passenger extends Person
{
	//Passenger class
	public Passenger (String fName, String lName, String gend)
	{
		firstName=fName;
		lastName=lName;
		gender=gend;
	}
}