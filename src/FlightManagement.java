
public abstract class FlightManagement {

	/**
	 * @param args
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Pilot Osman = new Pilot (14146,10000,"Osman","Osmanoglu","Male");
		Hostess Robin = new Hostess(10100,5000,"Robin","Stinson","Female");
		
		Pilot Barney = new Pilot (11234,20000,"Barney","Stinson","Male");
		Hostess Ted = new Hostess(9876,100,"Ted","Mosby","Male");
		
		Helicopter heli = new Helicopter(3,4,10,"IST",Osman,Robin);
		Airplane plane = new Airplane (4,10,20,"SAW",Barney,Ted);
		
		
		
		Passenger Ahmet = new Passenger("Ahmet","Ahmetoglu","Male");
		Passenger Hayri = new Passenger ("Hayri","Hayrioglu","Male");
		Passenger Hayriye = new Passenger ("Hayriye","Hayrioglu","Female");
		Passenger Enes = new Passenger ("Enes","Caylak","Male");
		Passenger Mert = new Passenger ("Mert","Issever","Male");
		Passenger Gokyar = new Passenger ("Gokyar","Sahin","Male");
		
		heli.passengerList.add(Ahmet);
		
		
		heli.takeOff();//there will not be enough passengers
		
		heli.passengerList.add(Hayri);
		heli.passengerList.add(Hayriye);// add passengers so enough passengers will be there
		
		heli.hostes.flightAttendatAnnouncemet();//make an announcement to warn passengers
		heli.takeOff();
		heli.landing();
		
		// now passengers will get into the plane
		plane.passengerList.add(Ahmet);
		plane.passengerList.add(Hayri);
		plane.passengerList.add(Hayriye);
	
		plane.takeOff();//there will not be enough passengers
		
		plane.passengerList.add(Enes);
		plane.passengerList.add(Mert);
		plane.passengerList.add(Gokyar);
		
		plane.hostes.flightAttendatAnnouncemet();
		plane.takeOff();//now it is able to take off with extra passengers
		plane.landing();
		
	}

}
