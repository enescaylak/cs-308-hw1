import java.util.ArrayList;
public abstract class Aircraft {

	/**
	 * @param args
	 */
	int capacity;
	public ArrayList<Passenger> passengerList=new ArrayList<Passenger>();
	Pilot pilot;
	Hostess hostes;
	int flightNumber;
	String destination;
	
	public Aircraft()
	{	}
	public Aircraft(int cap,int flightNum,String dest)
	{
		capacity=cap;
		flightNumber=flightNum;
		destination=dest;
	}
	void landing()
	{
		System.out.println("Landing procedure has completed.");
	}
	
	abstract void takeOff();
	abstract boolean checkPassengerList();

}
class Airplane extends Aircraft
{
	//Airplane class
	int engineCount;
	
	public Airplane()
	{ }
	public Airplane(int engcount,int cap,int flightNum,String dest,Pilot pil, Hostess host)
	{
		engineCount=engcount;
		capacity=cap;
		flightNumber=flightNum;
		destination=dest;
		pilot=pil;
		hostes=host;
	}
	void startEngines()
	{
		System.out.println(engineCount);
	}
	
	
	boolean checkPassengerList()
	{
		if(passengerList.size()>5 && passengerList.size()<capacity)// enough pasengers are in the plane
		{
			return true;
		}
		else if(passengerList.size()>capacity)//too much passenger
		{
			System.out.println("Too many Passengers, Can`t take off!");
			return false;
		}
		else 
		{
			System.out.println("Not Enough Passengers!");
		
			return false;
		}
	}
	
	
	void takeOff()
	{
		if(checkPassengerList())
		{
			startEngines();
			System.out.println("Take off procedure has completed.");
		}
		else
			System.out.println("Take off procedure could not be completed.");
	}
}
class Helicopter extends Aircraft
{
	///Helicopter class
	int propellerCount;
	
	public Helicopter()
	{}
	
	public Helicopter(int propcount,int cap,int flightNum,String dest,Pilot pil, Hostess host)
	{
		propellerCount=propcount;
		capacity=cap;
		flightNumber=flightNum;
		destination=dest;
		pilot=pil;
		hostes=host;
	}
	
	void startPropellers()
	{
		System.out.println(propellerCount);
	}
	
	boolean checkPassengerList()
	{
		if(passengerList.size()>2 && passengerList.size()<capacity)//enought passengers to take of mot much
		{
			return true;
		}
		else if(passengerList.size()>capacity)//too much passenger
		{
			System.out.println("Too many Passengers, Can`t take off!");
			return false;
		}
		else 
		{
			System.out.println("Not Enough Passengers!");
		
			return false;
		}
	}
	
	void takeOff()
	{
		if(checkPassengerList())
		{
			startPropellers();
			System.out.println("Take off procedure has completed.");
		}
		else
			System.out.println("Take off procedure could not be completed.");
	
	}
	
	
}